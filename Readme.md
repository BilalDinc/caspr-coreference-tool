[![codebeat badge](https://codebeat.co/badges/dfd893aa-d9b5-44e0-b44d-d2aab9ee3676)](https://codebeat.co/projects/bitbucket-org-knowlp-caspr-coreference-tool-master)

# Contents

* `caspr.py` CaspR Semi-Automatic Coreference Resolution Adjudication Tool based on Answer Set Programming

(submitted to LPNMR 2017)

Can process multiple files with CoNLL-format coreference information in last column.

# Dependencies

* Requires Clingo 5 or Gringo 5+Wasp 2.

  Clingo/Gringo/Clasp:
    https://github.com/potassco/clingo

  Wasp:
    https://github.com/alviano/wasp

# License

  Automatic Coreference Adjudication based on Answer Set Programming
  Copyright (C) 2015-2017 Peter Schüller <schueller.p@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
